# python virtual environment wrapper settings
export WORKON_HOME={{ virtualenvwrapper_base_dir }}
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5

# add the python virtualenvwrapper commands to the terminal session.
source /usr/bin/virtualenvwrapper.sh
