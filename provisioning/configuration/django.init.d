#!/bin/bash
# chkconfig: 2345 99 99
# description: execute the django development server as a service.

# Source function library.
. /etc/rc.d/init.d/functions

RETVAL=0
USER="vagrant"

DAEMON="workon django_app && /home/vagrant/django/app/manage.py runserver 0.0.0.0:8000"
LOG_DIR="{{ django_app_directory }}/log/"
LOG_FILE="${LOG_DIR}django.log"
LOCK_FILE="/var/lock/subsys/django-server"

do_start() {
  if [ ! -d "$LOG_DIR" ] ; then
    runuser -l "$USER" -c "mkdir $LOG_DIR"
  fi

  if [ ! -f "$LOG_FILE" ] ; then
    runuser -l "$USER" -c "touch $LOG_FILE"
  fi

  if [ ! -f "$LOCK_FILE" ] ; then
    echo "$DAEMON >> $LOG_FILE"
    echo -n "Starting django development server: "
    runuser -l "$USER" -c "$DAEMON >> $LOG_FILE 2>&1 &" && echo_success || echo_failure
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && touch $LOCK_FILE
  else
    echo "django development server is locked."
    RETVAL=1
  fi
}

do_stop() {
  echo -n "Stopping django development server: "
  pid=`ps -aefw | grep "manage.py" | grep "$USER" | grep -v " grep " | awk '{print $2}'`
  kill -9 $pid > /dev/null 2>&1 && echo_success || echo_failure
  RETVAL=$?
  echo
  [ $RETVAL -eq 0 ] && rm -f $LOCK_FILE
}

case "$1" in 
    start)
       do_start
       ;;
    stop)
       do_stop
       ;;
    restart)
       do_stop
       do_start
       ;;
    *)
       echo "Usage: $0 {start|stop|restart}"
esac

exit $RETVAL 
