# PostgreSQL database connection details
export POSTGRESQL_DBUSER={{ dbuser }}
export POSTGRESQL_DBPASSWORD={{ dbpassword }}
export POSTGRESQL_DBNAME={{ dbname }}

