var gulp = require('gulp');
var exec = require('child_process').exec;

gulp.task('watch', function() {
  gulp.watch('guest/**/*', ['sync'])
});

gulp.task('sync', function(cb) {
  exec(
    'vagrant provision --provision-with django-app-sync',
    function(err, stdout, stderr) {
      console.log(stdout);
      console.log(stderr);
      cb(err);
    }
  );
});

gulp.task('default', ['watch']);
