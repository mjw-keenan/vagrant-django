Vagrant - Django
================

My vagrant configuration for creating django development environments.

Components
----------

This vagrant box comes with the following setup/configuration:

- Centos/7 base VM
- Python3.5 installation
- pip3.5 installed
- virtualenv and virtualenvwrapper packages installed.
- PostgreSQL 9.6

A Python Virtual Environment is created on the machine with Django 1.10.5 and
psycopg2 2.6.2 installed by default.

A bare django project is created in the ``app`` directory located in the
vagrant user's home directory.
