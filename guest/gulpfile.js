var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var sass = require('gulp-sass');
var rename = require('gulp-rename');

var imagesGlob = '*.{gif,jpeg,jpg,png,svg}'
var appDirectoryStaticGlob = 'app/**/static/**/';

var globPatterns = {
  // return all images in the django app.
  images: `${appDirectoryStaticGlob}${imagesGlob}`,
  sass: `${appDirectoryStaticGlob}*.sass`,
};

function removeStaticPath(path) {
  // remove the '**/static/' portion from a filepath. Used when compiling
  // static files from individual django apps, to be placed in a 'static'
  // directory.
  var staticDirectoryString = '/static/';
  var staticDirectoryLength = staticDirectoryString.length;

  var staticPosition = path.dirname.search(staticDirectoryString);
  if (staticPosition !== -1) {
    var directoryNameLength = staticPosition + staticDirectoryLength
    path.dirname = path.dirname.substring(directoryNameLength);
  }
}
  
gulp.task('images-watch', function() {
  return gulp.watch(globPatterns.images, ['images-build'])
});
  
gulp.task('sass-watch', function() {
  return gulp.watch(globPatterns.sass, ['sass-build'])
});
  
gulp.task('images-build', function() {
  return gulp.src(globPatterns.images)
    .pipe(imagemin())
    .pipe(rename(removeStaticPath))
    .pipe(gulp.dest('static/'));
});

gulp.task('sass-build', function() {
  return gulp.src(globPatterns.sass)
    .pipe(sass().on('error', sass.logError))
    .pipe(rename(removeStaticPath))
    .pipe(gulp.dest('static/'));
});

gulp.task('build', [
  'images-build',
  'sass-build'
]);

gulp.task('watch', [
  'images-watch',
  'sass-watch'
]);

gulp.task('default', ['build', 'watch']);
